module.exports = {
  purge: ['./dist/*.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    maxWidth: {
      '1/4': '25%',
      half: '50%',
      '3/4': '75%',
    },
    maxHeight: {
      '1/4': '25%',
      half: '50%',
      '3/4': '75%',
    },
    extend: {
      keyframes: {
        'bg-loading': {
          '0%': {
            opacity: '0',
          },
          '100%': { opacity: '1' },
        },
        'modal-loading': {
          from: {
            opacity: '0',
            'margin-top': '-50px',
          },
          to: {
            opacity: '1',
            'margin-top': '0',
          },
        },
      },
      animation: {
        bg: 'bg-loading .5s ease-in-out',
        modal: 'modal-loading .5s ease-in-out',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
