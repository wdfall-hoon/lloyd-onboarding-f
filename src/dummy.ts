export interface user {
  username: string;
  bio: string;
  image: string;
  following: boolean;
}

export interface article {
  slug: string;
  title: string;
  description: string;
  body: string;
  tagList: Array<string>;
  createdAt: string;
  updatedAt: string;
  favorited: boolean;
  favoritesCount: number;
  author: user;
}

export interface articles {
  articles: Array<article>;
  articlesCount: number;
}

export interface comment {
  id: number;
  createdAt: string;
  updatedAt: string;
  body: string;
  author: user;
}
