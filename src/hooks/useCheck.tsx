import React, { useEffect, useState } from 'react';
import { useRecoilValue } from 'recoil';
import { User } from '../utils/Auth';

export const useCheck = (name?: string): Array<boolean> => {
  const userInfo = useRecoilValue(User);
  const [isLogin, setLogin] = useState<boolean>();
  const [isAuthor, setAuthor] = useState<boolean>();

  useEffect(() => {
    setLogin(userInfo.username ? true : false);
    setAuthor(userInfo.username === name);
  }, [name]);

  return [isLogin!, isAuthor!];
};
