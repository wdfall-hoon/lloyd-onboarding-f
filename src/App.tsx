import React, { useEffect, useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import {
  HomePage,
  ArticlePage,
  ArticleWritePage,
  LoginPage,
  RegisterPage,
  ArticleUpdatePage,
} from './pages';
import { Menu, PrivateRoute, AuthRoute } from './components/';
import { useSetRecoilState } from 'recoil';
import { User } from './utils/Auth';
import { getUser } from './apis/article-services';

const App = () => {
  const [isLoading, setLoading] = useState(false);
  const setUser = useSetRecoilState(User);
  useEffect(() => {
    getUser()
      .then(({ data: { user: userInfo } }) => {
        setUser(userInfo);
        setLoading(true);
      })
      .catch(() => setLoading(true));
  }, []);
  return isLoading ? (
    <>
      <BrowserRouter>
        <Menu />
        <Switch>
          <PrivateRoute path="/article/write" component={ArticleWritePage} />
          <PrivateRoute path="/article/update/:id" component={ArticleUpdatePage} />
          <Route path="/article/:id" component={ArticlePage} />
          <AuthRoute path="/register" component={RegisterPage} />
          <AuthRoute path="/login" component={LoginPage} />
          <Route exact path="/" component={HomePage} />
        </Switch>
      </BrowserRouter>
    </>
  ) : (
    <div className="flex w-full h-full">
      <div className="mx-auto my-20">Loading...</div>
    </div>
  );
};

export default App;
