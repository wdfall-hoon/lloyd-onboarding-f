import * as React from 'react';
import * as ReactDom from 'react-dom';
import './global.css';
import { RecoilRoot } from 'recoil';
import App from './App';

ReactDom.render(
  <RecoilRoot>
    <App />
  </RecoilRoot>,
  document.getElementById('root'),
);
