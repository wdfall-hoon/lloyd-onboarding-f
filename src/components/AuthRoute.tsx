import React from 'react';
import { Route, Redirect } from 'react-router';
import { useCheck } from '../hooks/useCheck';

export const AuthRoute = ({ component: Component, ...props }: any) => {
  const [isLogin] = useCheck();
  return (
    <Route
      {...props}
      render={props =>
        !isLogin ? (
          <Component {...props} />
        ) : (
          <>
            <Redirect to="/" />
            {alert('이미 로그인이 되어있습니다.')}
          </>
        )
      }
    />
  );
};
