import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { deleteArticle } from '../apis/article-services';
import { deleteButton, previewButton } from '../cssClass';
import { article } from '../dummy';
import { useCheck } from '../hooks/useCheck';
import { momentFromNow } from '../utils/moment';

interface previewProps {
  content: article;
  comment: (isOpen: boolean, id: string) => void;
}

export const Preview = ({ content, comment }: previewProps) => {
  const { push } = useHistory();
  const [, isAuthor] = useCheck(content.author.username);

  return (
    <div className="animate-bg flex max-w-half w-full h-60 justify-between mx-auto rounded-3xl m-16 gap-5 bg-white shadow-2xl">
      <button
        onClick={() => push(`/article/${content.slug}`)}
        className="transition max-w-3/4 h-full w-full duration-500 rounded-3xl p-4 bg-gradient-to-l from-white hover:bg-blue-50"
      >
        <span className="p-5 w-max">
          <p className="text-left font-bold text-4xl text-left truncate">{content.title}</p>
          <p className="my-2 font-lightbold text-xl text-left text-gray-400 truncate">
            {content.description}
          </p>
          <p className="text-left text-gray-500 my-10 text-2xl">
            by {content.author.username} - {momentFromNow(content.createdAt)}
          </p>
        </span>
      </button>
      <div className="flex h-full justify-between flex-col items-center p-7 w-max rounded-3xl p-4">
        <button className={previewButton} onClick={() => comment(true, content.slug)}>
          Comments
        </button>
        {isAuthor ? (
          <>
            <Link to={`/article/update/${content.slug}`} className={previewButton}>
              Update
            </Link>
            <button
              className={deleteButton}
              onClick={() => {
                if (confirm('정말 삭제하시겠습니까?')) {
                  deleteArticle(content.slug);
                  push('/');
                  window.location.reload();
                } else return;
              }}
            >
              Delete
            </button>
          </>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};
