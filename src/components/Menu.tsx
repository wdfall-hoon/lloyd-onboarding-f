import React from 'react';
import { NavLink } from 'react-router-dom';
import { authButton, createButton } from '../cssClass';
import { useCheck } from '../hooks/useCheck';

export const Menu = () => {
  const [isLogin] = useCheck();
  return (
    <div className="sticky mx-auto top-0 p-2 bg-white shadow-lg w-full h-20 flex justify-between">
      <span className="w-full m-auto font-bold text-4xl text-center text-purple-500">
        <NavLink exact to="/">
          Without CRA SNS
        </NavLink>
      </span>
      {isLogin ? (
        <span className="w-full m-auto font-bold text-2xl text-center">
          <NavLink className={createButton} to="/article/write">
            Write
          </NavLink>
          <button
            className="mx-5 font-bold"
            onClick={() => {
              sessionStorage.clear();
              window.location.reload();
            }}
          >
            Sign Out
          </button>
        </span>
      ) : (
        <span className="w-full m-auto font-bold text-2xl text-center">
          <NavLink className={authButton} to="/login">
            Sign In
          </NavLink>
        </span>
      )}
    </div>
  );
};
