import React, { useEffect } from 'react';

interface Props {
  open: boolean;
  header: string;
  close: () => void;
  children: React.ReactNode;
}

export const Modal = ({ open, close, header, children }: Props) => {
  useEffect(() => {
    if (open) document.body.style.overflow = 'hidden';
    else document.body.style.overflow = 'unset';
  }, [open]);
  return (
    <div
      className={`animate-bg bg-gray-500 top-0 left-0 bg-opacity-75 fixed w-full h-full flex ${
        open ? `flex items-center` : 'hidden'
      }`}
      onClick={close}
    >
      {open ? (
        <section
          className="animate-modal max-w-half w-full mx-auto bg-white shadow-xl none rounded-xl"
          onClick={e => e.stopPropagation()}
        >
          <header className="relative p-2 rounded-xl bg-white font-bold">{header}</header>
          <main>{children}</main>
        </section>
      ) : (
        <></>
      )}
    </div>
  );
};
