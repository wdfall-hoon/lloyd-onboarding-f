import React from 'react';
import { Route, Redirect } from 'react-router';

export const PrivateRoute = ({ component: Component, ...props }: any) => {
  // const [isLogin] = useCheck();
  const isLogin = sessionStorage.getItem('token');
  return (
    <Route
      {...props}
      render={props =>
        isLogin ? (
          <Component {...props} />
        ) : (
          <>
            <Redirect to="/login" />
            {alert('로그인이 필요한 서비스입니다.')}
          </>
        )
      }
    />
  );
};
