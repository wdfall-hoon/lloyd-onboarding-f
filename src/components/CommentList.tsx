import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { CommentCard } from '.';
import { createComment, getCommentList } from '../apis/article-services';
import { authButton, formError, formInput } from '../cssClass';
import { comment } from '../dummy';
import { useCheck } from '../hooks/useCheck';

interface Props {
  id: string;
}

interface formData {
  body: string;
}

export const CommentList = ({ id }: Props) => {
  const [commentList, setCommentList] = useState<Array<comment>>([]);
  const [refresh, setRefresh] = useState(true);
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
  } = useForm();

  const onSubmit = (data: formData) => {
    createComment(id, data)
      .then(() => {
        setRefresh(!refresh);
        reset();
      })
      .catch(err => alert(err));
  };

  const [isLogin] = useCheck();

  useEffect(() => {
    getCommentList(id).then(({ data: { comments } }) => setCommentList(comments.reverse()));
  }, [refresh]);

  return (
    <>
      <div className="max-h-half h-full overflow-y-scroll flex flex-col p-5 gap-5">
        {commentList.length
          ? commentList.map((comment, index) => (
              <CommentCard
                key={index}
                id={id}
                comment={comment}
                refresh={() => setRefresh(!refresh)}
              />
            ))
          : '댓글이 없습니다.'}
      </div>
      {isLogin ? (
        <form className="bottom-0 m-0 p-5 rounded-xl bg-white" onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-4">
            <textarea
              {...register('body', { required: true })}
              className={formInput}
              placeholder="Comment"
            />
            {errors.comment && <p className={formError}>Comment is required.</p>}
          </div>
          <div className="tems-center justify-between">
            <button className={authButton}>Post</button>
          </div>
        </form>
      ) : (
        <></>
      )}
    </>
  );
};
