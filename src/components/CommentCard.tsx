import React from 'react';
import { deleteComment } from '../apis/article-services';
import { comment } from '../dummy';
import { useCheck } from '../hooks/useCheck';
import { momentFromNow } from '../utils/moment';

interface Props {
  id: string;
  comment: comment;
  refresh: () => void;
}

export const CommentCard = ({ id, comment, refresh }: Props) => {
  const [, isAuthor] = useCheck(comment.author.username);
  return (
    <span className={`w-max p-5 bg-white rounded-xl max-w-3/4 w-max shadow`}>
      <span>
        <p className="font-bold text-xl break-words">{comment.author.username}</p>
        <p className="break-words whitespace-pre-wrap">{comment.body}</p>
        <p className="mt-3 text-gray-400">{momentFromNow(comment.createdAt)}</p>
      </span>
      {isAuthor ? (
        <button
          onClick={() => {
            if (confirm('정말 삭제하시겠습니까?'))
              deleteComment(id, comment.id).then(() => refresh());
          }}
        >
          Delete
        </button>
      ) : (
        <></>
      )}
    </span>
  );
};
