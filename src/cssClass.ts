export const createButton =
  'text-center w-full h-max bg-purple-500 hover:bg-purple-700 text-white py-2 px-4 rounded-md focus:outline-none focus:shadow-outline';

export const deleteButton =
  'text-center w-full h-max bg-red-400 hover:bg-red-600 text-white py-2 px-4 rounded-md focus:outline-none focus:shadow-outline';

export const authButton =
  'text-center w-full h-max bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded-md focus:outline-none focus:shadow-outline';

export const previewButton =
  'text-center w-full h-max bg-white shadow hover:bg-gray-200 py-2 px-4 border border-solid border-gray-200 rounded-md focus:outline-none focus:shadow-outline';

export const formLabel = 'block text-gray-700 text-sm font-bold mb-2';

export const formInput =
  'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline';

export const formError = 'font-bold text-red-500';
