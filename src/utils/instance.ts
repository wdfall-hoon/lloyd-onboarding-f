import axios from 'axios';

const instance = axios.create({
  baseURL: `http://14.36.51.141:3000`,
  timeout: 1000,
});

instance.interceptors.request.use(
  config => {
    config.headers.Authorization = `Token ${sessionStorage.getItem('token')}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

instance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    return Promise.reject(error);
  },
);

export default instance;
