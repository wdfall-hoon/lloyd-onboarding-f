import moment from 'moment';
import 'moment/locale/ko';

export const momentFromNow = (date: string) => moment(date).fromNow();
