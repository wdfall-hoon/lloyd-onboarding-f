import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { deleteArticle, getArticle } from '../apis/article-services';
import { CommentList } from '../components/';
import { createButton, deleteButton } from '../cssClass';
import { article } from '../dummy';
import { useCheck } from '../hooks/useCheck';
import { momentFromNow } from '../utils/moment';

export const ArticlePage = () => {
  const { id } = useParams<{ id: string }>();
  const [article, setArticle] = useState<article>();
  const [isLoading, setLoading] = useState(false);
  const { push } = useHistory();
  useEffect(() => {
    getArticle(id).then(({ data: { article: content } }) => {
      setArticle(content);
      setLoading(true);
    });
  }, [id]);
  const [, isAuthor] = useCheck(article?.author.username);
  return isLoading ? (
    article ? (
      <div className="flex flex-col max-w-half mx-auto my-20 gap-5">
        <div className="animate-modal">
          <div className="p-2 w-full p-16 h-max rounded-3xl shadow-2xl bg-white">
            <span className="flex justify-between my-auto">
              <p className="font-bold text-6xl mb-6 break-words w-full">{article.title}</p>
              {isAuthor ? (
                <span className="gap-5 flex h-max m-auto">
                  <Link to={`/article/update/${id}`} className={createButton}>
                    Update
                  </Link>
                  <button
                    className={deleteButton}
                    onClick={() => {
                      if (confirm('정말 삭제하시겠습니까?')) {
                        deleteArticle(id);
                        push('/');
                        window.location.reload();
                      } else return;
                    }}
                  >
                    Delete
                  </button>
                </span>
              ) : (
                <></>
              )}
            </span>
            <p className="text-gray-500 text-3xl mb-10 break-words">
              by {article.author.username} - {momentFromNow(article.createdAt)}
            </p>
            <p className="text-4xl break-words whitespace-pre-wrap">{article.body}</p>
            <p className="font-bold text-xl mb-5 mt-20">Comment</p>
            <CommentList id={id} />
          </div>
        </div>
      </div>
    ) : (
      <>404 not found</>
    )
  ) : (
    <></>
  );
};
