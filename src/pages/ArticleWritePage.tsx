import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { createArticle } from '../apis/article-services';
import { authButton, formError, formInput, formLabel } from '../cssClass';

interface formData {
  title: string;
  description: string;
  body: string;
}

export const ArticleWritePage = () => {
  const { push } = useHistory();
  const {
    formState: { errors },
    register,
    handleSubmit,
  } = useForm();

  const onSubmit = (data: formData) => {
    createArticle(data)
      .then(({ data: { article } }) => push(`/article/${article.slug}`))
      .catch(err => alert(err));
  };

  return (
    <div className="w-full h-full flex">
      <div className="m-auto">
        <form className="mx-auto w-max h-max p-20 shadow-2xl" onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-4">
            <label className={formLabel}>Title</label>
            <input
              {...register('title', { required: true })}
              className={formInput}
              placeholder="title"
            />
            {errors.title && <p className={formError}>Title is required.</p>}
          </div>
          <div className="mb-4">
            <label className={formLabel}>Description</label>
            <input
              {...register('description', { required: true })}
              className={formInput}
              placeholder="description"
            />
            {errors.description && <p className={formError}>Descriotion is required.</p>}
          </div>
          <div className="mb-6">
            <label className={formLabel}>Body</label>
            <textarea
              {...register('body', { required: true })}
              className={formInput}
              placeholder="body"
            />
            {errors.body && <p className={formError}>Body is required.</p>}
          </div>
          <div className="flex items-center justify-between">
            <button className={authButton}>Post</button>
          </div>
        </form>
      </div>
    </div>
  );
};
