import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { Register } from '../apis/article-services';
import { authButton, formError, formInput, formLabel } from '../cssClass';

interface formData {
  username: string;
  email: string;
  password: string;
}

export const RegisterPage = () => {
  const { push } = useHistory();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: formData) => {
    Register(data)
      .then(({ data: { user: userInfo } }) => {
        push('/');
        sessionStorage.setItem('token', userInfo.token);
        window.location.reload();
      })
      .catch(err => alert('사용할 수 없는 정보입니다.'));
  };

  return (
    <div className="w-full h-full flex">
      <div className="m-auto">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="place-self-center shadow-2xl bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <label className={formLabel}>Name</label>
            <input
              {...register('username', { required: true })}
              className={formInput}
              type="text"
              placeholder="Name"
            />
            {errors.username && <p className={formError}>Name is required.</p>}
          </div>
          <div className="mb-4">
            <label className={formLabel}>Email</label>
            <input
              {...register('email', { required: true })}
              className={formInput}
              type="email"
              placeholder="Email"
            />
            {errors.email && <p className={formError}>Email is required.</p>}
          </div>
          <div className="mb-6">
            <label className={formLabel}>Password</label>
            <input
              {...register('password', { required: true })}
              className={formInput}
              type="password"
              placeholder="******************"
            />
            {errors.password && <p className={formError}>Password is required.</p>}
          </div>
          <div className="flex items-center justify-between">
            <button className={authButton}>Sign Up</button>
          </div>
        </form>
      </div>
    </div>
  );
};
