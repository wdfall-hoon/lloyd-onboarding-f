import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Redirect, useHistory, useParams } from 'react-router';
import { getArticle, updateArticle } from '../apis/article-services';
import { authButton, formError, formInput, formLabel } from '../cssClass';
import { useCheck } from '../hooks/useCheck';

interface formData {
  title: string;
  description: string;
  body: string;
}

export const ArticleUpdatePage = () => {
  const [isLoading, setLoading] = useState(false);
  const { id } = useParams<{ id: string }>();
  const [author, setAuthor] = useState('');
  const { push } = useHistory();
  const {
    setValue,
    formState: { errors },
    register,
    handleSubmit,
  } = useForm();

  useEffect(() => {
    getArticle(id!).then(({ data: { article: content } }) => {
      setAuthor(content.author.username);
      setValue('title', content.title);
      setValue('description', content.description);
      setValue('body', content.body);
      setLoading(true);
    });
  }, [id]);

  const [, isAuthor] = useCheck(author);

  const onSubmit = (data: formData) => {
    updateArticle(id, data)
      .then(({ data: { article } }) => push(`/article/${article.slug}`))
      .catch(err => alert(err));
  };

  return isLoading ? (
    isAuthor ? (
      <div className="w-full h-full flex">
        <div className="m-auto">
          <form className="mx-auto w-max h-max p-20 shadow-2xl" onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-4">
              <label className={formLabel}>Title</label>
              <input
                {...register('title', { required: true })}
                className={formInput}
                placeholder="title"
              />
              {errors.title && <p className={formError}>Title is required.</p>}
            </div>
            <div className="mb-4">
              <label className={formLabel}>Description</label>
              <input
                {...register('description', { required: true })}
                className={formInput}
                placeholder="description"
              />
              {errors.description && <p className={formError}>Descriotion is required.</p>}
            </div>
            <div className="mb-6">
              <label className={formLabel}>Body</label>
              <textarea
                {...register('body', { required: true })}
                className={formInput}
                placeholder="body"
              />
              {errors.body && <p className={formError}>Body is required.</p>}
            </div>
            <div className="flex items-center justify-between">
              <button className={authButton}>Update</button>
            </div>{' '}
          </form>
        </div>
      </div>
    ) : (
      <>
        {alert('해당 게시글을 수정할 권한이 없습니다!')} <Redirect to="/" />
      </>
    )
  ) : (
    <></>
  );
};
