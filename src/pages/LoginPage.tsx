import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { Login } from '../apis/article-services';
import { authButton, formError, formInput, formLabel } from '../cssClass';

interface formData {
  email: string;
  password: string;
}

export const LoginPage = () => {
  const { push } = useHistory();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: formData) => {
    Login(data)
      .then(({ data: { user: userInfo } }) => {
        push('/');
        sessionStorage.setItem('token', userInfo.token);
        window.location.reload();
      })
      .catch(err => alert('계정 정보가 올바르지 않습니다.'));
  };

  return (
    <div className="w-full h-full flex">
      <div className="m-auto">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="shadow-2xl bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <label className={formLabel}>Email</label>
            <input
              {...register('email', { required: true })}
              className={formInput}
              type="email"
              placeholder="Email"
            />
            {errors.email && <p className={formError}>Email is required.</p>}
          </div>
          <div className="mb-6">
            <label className={formLabel}>Password</label>
            <input
              {...register('password', { required: true })}
              className={formInput}
              type="password"
              placeholder="******************"
            />
            {errors.password && <p className={formError}>Password is required.</p>}
          </div>
          <div className="flex items-center justify-between">
            <button className={authButton}>Sign In</button>
          </div>
          <Link
            to="/register"
            className="text-center mt-5 inline-block w-full align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
          >
            need sign up?
          </Link>
        </form>
      </div>
    </div>
  );
};
