import React, { useEffect, useState } from 'react';
import { getArticleList } from '../apis/article-services';
import { Preview, Modal, CommentList } from '../components/';
import { articles } from '../dummy';

export const HomePage = () => {
  const [articleList, setArticleList] = useState<articles>({ articles: [], articlesCount: 0 });
  const [isLoading, setLoading] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [id, setId] = useState('');
  const { articles, articlesCount } = articleList;
  useEffect(() => {
    getArticleList().then(({ data }) => {
      setArticleList(data);
      setLoading(true);
    });
  }, []);
  return isLoading ? (
    <div className="max-w-screen-xl mx-auto">
      <div>
        {articlesCount ? (
          articles.map((article, index) => (
            <Preview
              key={index}
              content={article}
              comment={(isOpen: boolean, id: string) => {
                setOpen(isOpen);
                setId(id);
              }}
            />
          ))
        ) : (
          <p className="font-bold text-4xl mx-auto w-max h-max p-4 m-4 rounded-3xl shadow-2xl bg-gradient-to-r from-gray-50">
            현재 게시된 글이 없습니다.
          </p>
        )}
        <Modal open={isOpen} close={() => setOpen(false)} header="Comments">
          <CommentList id={id} />
        </Modal>
      </div>
    </div>
  ) : (
    <></>
  );
};
