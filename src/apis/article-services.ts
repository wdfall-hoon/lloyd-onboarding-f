import api from '../utils/instance';

interface auth {
  email: string;
  password: string;
  username?: string;
}

interface post {
  title: string;
  description: string;
  body: string;
}

interface comment {
  body: string;
}

export const getArticle = (id: string) => api.get(`/api/articles/${id}`);

export const getArticleList = () => api.get('/api/articles');

export const getCommentList = (id: string) => api.get(`/api/articles/${id}/comments`);

export const createArticle = (article: post) =>
  api.post('/api/articles', {
    article,
  });

export const createComment = (id: string, comment: comment) =>
  api.post(`/api/articles/${id}/comments`, {
    comment,
  });

export const deleteArticle = (id: string) => api.delete(`/api/articles/${id}`);

export const deleteComment = (articleId: string, commentId: number) =>
  api.delete(`/api/articles/${articleId}/comments/${commentId}`);

export const updateArticle = (id: string, article: post) =>
  api.put(`/api/articles/${id}`, {
    article,
  });

export const Login = (user: auth) =>
  api.post('/api/users/login', {
    user,
  });

export const Register = (user: auth) =>
  api.post('/api/users', {
    user,
  });

export const getUser = () => api.get('/api/user');
