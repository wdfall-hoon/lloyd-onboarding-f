module.exports = env => {
  return require(`./webpack.${env.dev ? 'dev' : 'prod'}.js`);
};
